let express = require('express')
let app = express();

let http = require('http');
let server = http.Server(app);

let socketIO = require('socket.io');
let io = socketIO(server);

const port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log(`started on port: ${port}`);
});

io.on('connect', (socket) => {
  console.log(`user connected`, socket.client.id);

  socket.on('message', message => {
    console.log(message);
    io.emit('message', message);
  });

  socket.on('disconnect', function () {
    console.log('user disconnected');
  });
});
