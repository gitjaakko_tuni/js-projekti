Projektin tekijät: Jaakko Pullinen & Petri Pellinen


Ohjeet: Valitse itsellesi projektikansio ja komentoikkunassa suorita "git clone https://gitlab.com/gitjaakko_tuni/js-projekti.git"

Tämän jälkeen suorita seuraavat komennot: cd js-projekti/chat-be && npm i

cd .. && cd socketchatapp && npm i

Kun molemmat on asennettu suorita "npm start" molemmissa pääkansioissa (socketchatapp ja chat-be)

Sovellus pyörii nyt oletuksena :4200 & 3000 porteissa.