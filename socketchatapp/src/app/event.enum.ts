export enum EventEnum {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect'
}

export enum MessageTypesEnum {
  MESSAGE = 'message'
}

export enum ActionEnum {
  JOINED,
  LEFT,
  RENAME,
  MESSAGE
}

