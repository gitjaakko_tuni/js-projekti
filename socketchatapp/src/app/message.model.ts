import { ActionEnum } from './event.enum';

export interface Message {
  id: string;
  from: User;
  message: string;
  action: ActionEnum;
}

export interface User {
  id: number;
  name: string;
}

