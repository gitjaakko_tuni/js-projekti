import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { MessageTypesEnum } from './event.enum';
import { Message } from './message.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private url = 'http://localhost:3000';
  private socket: SocketIOClient.Socket;

  constructor() { }

  setupConnection() {

    // initial socket connection initialization
    this.socket = io(this.url);

    // this.socket.on(EventEnum.CONNECT, () => {
    //   console.log('on client connection');
    // });
  }

  sendMessage(msg: Message) {
    this.socket.emit(MessageTypesEnum.MESSAGE, msg);
  }

  onNewMessage() {
    return new Observable((observer) => {
      this.socket.on(MessageTypesEnum.MESSAGE, (message: Message) => {
        observer.next(message);
      });
    });
  }

}
