import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { Message, User } from './message.model';
import { NgForm } from '@angular/forms';
import { ActionEnum } from './event.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  action = ActionEnum;
  message: string;
  messageId: string;
  messages: Message[] = [];

  openEdit = false;
  user: User = {
    id: null,
    name: null
  };

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit() {

    this.openEditForm();
    this.chatService.setupConnection();

    this.chatService
      .onNewMessage()
      .subscribe((message: Message) => {
        this.messages.push(message);
      });
  }

  sendMessage() {
    const randomId = Math.floor(Math.random() * 100000).toString();
    this.messageId = randomId;
    const newMessage: Message = {
      id: randomId,
      from: this.user,
      message: this.message,
      action: ActionEnum.MESSAGE
    };
    this.chatService.sendMessage(newMessage);
    this.message = '';
  }

  openEditForm() {
    this.openEdit = true;
  }

  newUser(form: NgForm) {
    const randomId = Math.floor(Math.random() * 10000);
    this.user.name = form.value.username;
    this.user.id = randomId;
    this.openEdit = false;

    this.newUserCreated(this.user);
  }

  newUserCreated(user: User) {
    const newMessage: Message = {
      id: this.messageId,
      from: user,
      message: '',
      action: ActionEnum.JOINED
    };
    this.chatService.sendMessage(newMessage);
  }
}
